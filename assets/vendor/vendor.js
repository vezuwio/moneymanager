/***************************************
 *                                     *
 *             JavaScript              *
 *                                     *
 * *************************************/

import $ from 'jquery';
global.$ = $;
import 'bootstrap';
import 'pc-bootstrap4-datetimepicker/build/js/bootstrap-datetimepicker.min.js'


/***************************************
 *                                     *
 *                 CSS                 *
 *                                     *
 * *************************************/

// import 'bootstrap/dist/css/bootstrap-reboot.css';
// import 'bootstrap/dist/css/bootstrap-grid.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css';
