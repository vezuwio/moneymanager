<?php

namespace App\Controller;

use App\DTO\ExpenseDTO;
use App\Entity\PaymentMethod;
use App\Entity\Bank;
use App\Entity\Expense;
use App\Form\PaymentMethodType;
use App\Form\BankType;
use App\Form\ExpenseType;
use App\Repository\PaymentMethodRepository;
use App\Repository\BankRepository;
use App\Repository\ExpenseRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/paymentMethod")
 */
class PaymentMethodController extends Controller
{

    /** @var PaymentMethodRepository */
    protected $PaymentMethod;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    public function __construct(PaymentMethodRepository $PaymentMethodRepository, EntityManagerInterface $entityManager)
    {
        $this->PaymentMethod = $PaymentMethodRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/list", name="payment_method_list")
     * @Template
     */
    public function list()
    {

        $entities = $this->PaymentMethod->findAll();

        return [
            'entities' => $entities,
        ];
    }

    /**
     * @Route("/add", name="payment_method_add")
     * @Template
     */
    public function add(Request $request)
    {
        $expense = new PaymentMethod();

        $form = $this->createForm(PaymentMethodType::class, $expense);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->entityManager->persist($expense);
            $this->entityManager->flush();

            $this->addFlash('success', 'Konto bankowe zostało dodane');

            return $this->redirectToRoute('payment_method_add');
        }

        return [
            'form' => $form->createView(),
        ];
    }
}
