<?php

namespace App\Controller;

use App\DTO\ExpenseDTO;
use App\Entity\Bank;
use App\Entity\Expense;
use App\Form\BankType;
use App\Form\ExpenseType;
use App\Repository\BankRepository;
use App\Repository\ExpenseRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/bank")
 */
class BankController extends Controller
{

    /** @var BankRepository */
    protected $bank;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    public function __construct(BankRepository $bankRepository, EntityManagerInterface $entityManager)
    {
        $this->bank = $bankRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/list", name="bank_list")
     * @Template
     */
    public function list()
    {

        $entities = $this->bank->findAll();

        return [
            'entities' => $entities,
        ];
    }

    /**
     * @Route("/add", name="bank_add")
     * @Template
     */
    public function add(Request $request)
    {
        $expense = new Bank();

        $form = $this->createForm(BankType::class, $expense);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->entityManager->persist($expense);
            $this->entityManager->flush();

            $this->addFlash('success', 'Bank został dodany');

            return $this->redirectToRoute('bank_add');
        }

        return [
            'form' => $form->createView(),
        ];
    }
}
