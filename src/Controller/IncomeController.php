<?php

namespace App\Controller;

use App\Entity\Income;
use App\Form\IncomeType;
use App\Repository\IncomeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/income")
 */
class IncomeController extends Controller
{

    /** @var IncomeRepository */
    protected $income;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    public function __construct(IncomeRepository $incomeRepository, EntityManagerInterface $entityManager)
    {
        $this->income = $incomeRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name="income_list")
     * @Template
     */
    public function list()
    {

        $incomes = $this->income->findAll();

        return [
            'income' => $incomes,
        ];
    }

    /**
     * @Route("/add", name="income_add")
     * @Template
     */
    public function add(Request $request)
    {

        $entity = new Income();

        $form = $this->createForm(IncomeType::class, $entity);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            $this->addFlash('success', 'Dochód został zapisany');

            return $this->redirectToRoute('income_add');
        }

        return [
            'form' => $form->createView(),
        ];
    }

}
