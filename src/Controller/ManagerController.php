<?php

namespace App\Controller;

use App\Repository\ExpenseRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/account")
 */
class ManagerController extends Controller
{
    /**
     * @Route("/dashboard", name="manager_dashboard")
     * @Template
     */
    public function dashboard()
    {
        return [];
    }
}
