<?php

namespace App\Controller;

use App\DTO\CategoryDTO;
use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Security("has_role('ROLE_ADMIN')")
 * @Route("/category")
 */
class CategoryController extends Controller
{

    /**
     * @var CategoryRepository
     */
    protected $category;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    public function __construct(CategoryRepository $categoryRepository, EntityManagerInterface $entityManager)
    {
        $this->category = $categoryRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/list", name="category_list")
     * @Template
     */
    public function list()
    {

        $categories = $this->category->findAll();

        return [
            'categories' => $categories,
        ];
    }

    /**
     * @Route("/add", name="category_add")
     * @Template
     */
    public function add(Request $request)
    {

        $category = new Category();

        $form = $this->createForm(CategoryType::class, $category);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->entityManager->persist($category);
            $this->entityManager->flush();

            $this->addFlash('success', 'Kategoria została dodana');

            return $this->redirectToRoute('category_add');
        }

        return [
            'form' => $form->createView(),
        ];
    }
}
