<?php

namespace App\Controller;

use App\Repository\ExpenseRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{

    /**
     * @Route("/", name="start_page")
     * @Template
     */
    public function index()
    {

        if ($this->getUser()) {
            return $this->redirectToRoute('manager_dashboard');
        }

        return [

        ];
    }
}
