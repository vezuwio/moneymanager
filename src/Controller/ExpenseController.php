<?php

namespace App\Controller;

use App\DTO\ExpenseDTO;
use App\Entity\Expense;
use App\Form\ExpenseType;
use App\Repository\ExpenseRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/expense")
 */
class ExpenseController extends Controller
{

    /** @var ExpenseRepository */
    protected $expense;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    public function __construct(ExpenseRepository $expenseRepository, EntityManagerInterface $entityManager)
    {
        $this->expense = $expenseRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name="expense_list")
     * @Template
     */
    public function list()
    {

        $expenses = $this->expense->findAll();

        return [
            'expenses' => $expenses,
        ];
    }

    /**
     * @Route("/add", name="expense_add")
     * @Template
     */
    public function add(Request $request)
    {
        $expense = new Expense();

        $form = $this->createForm(ExpenseType::class, $expense);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->entityManager->persist($expense);
            $this->entityManager->flush();

            $this->addFlash('success', 'Wydatek został zapisany');

            return $this->redirectToRoute('expense_add');
        }

        return [
            'form' => $form->createView(),
        ];
    }
}
