<?php

namespace App\Entity;

use App\Entity\Traits\TimestampableTrait;
use Doctrine\ORM\Mapping as ORM;

class BudgetEntity implements EntityInterface
{

    use TimestampableTrait;

    const TYPE_ACCOUNT       = 'Bank Account';
    const TYPE_CASH          = 'Cash';
    const TYPE_DEBIT_CARD    = 'Debit Card';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", nullable=false)
     */
    private $user;

    /**
     * @var float
     *
     * @ORM\Column(name="transaction_date", type="float", nullable=false)
     */
    private $moneyAmount;

    /**
     * @var Category
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumn(name="category_id", nullable=true)
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="transaction_date", type="string", nullable=true)
     */
    private $moneyType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="transaction_date", type="datetime", nullable=true)
     */
    private $transactionDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PaymentMethod")
     * @ORM\JoinColumn(name="payment_method_id", referencedColumnName="id")
     */
    private $paymentMethod;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return float
     */
    public function getMoneyAmount(): ?float
    {
        return $this->moneyAmount;
    }

    /**
     * @return Category
     */
    public function getCategory(): ?Category
    {
        return $this->category;
    }

    /**
     * @return string
     */
    public function getMoneyType(): ?string
    {
        return $this->moneyType;
    }

    /**
     * @return \DateTime
     */
    public function getTransactionDate(): ?\DateTime
    {
        return $this->transactionDate;
    }

    /**
     * @param Category $category
     * @return BudgetEntity
     */
    public function setCategory(Category $category): BudgetEntity
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @param string $moneyType
     * @return BudgetEntity
     */
    public function setMoneyType(string $moneyType): BudgetEntity
    {
        $this->moneyType = $moneyType;
        return $this;
    }

    /**
     * @param \DateTime $transactionDate
     * @return BudgetEntity
     */
    public function setTransactionDate(\DateTime $transactionDate): BudgetEntity
    {
        $this->transactionDate = $transactionDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * @param mixed $paymentMethod
     * @return BudgetEntity
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
        return $this;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @param float $moneyAmount
     */
    public function setMoneyAmount(float $moneyAmount): void
    {
        $this->moneyAmount = $moneyAmount;
    }

}