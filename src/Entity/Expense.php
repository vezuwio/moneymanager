<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Expense
 *
 *
 * @ORM\Table(name="expense")
 * @ORM\Entity(repositoryClass="App\Repository\ExpenseRepository")
 */
class Expense extends BudgetEntity implements EntityInterface
{
}