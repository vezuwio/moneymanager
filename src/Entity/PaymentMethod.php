<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PaymentMethod
 *
 *
 * @ORM\Table(name="payment_method")
 * @ORM\Entity(repositoryClass="App\Repository\PaymentMethodRepository")
 */
class PaymentMethod
{

    const TYPE_ACCOUNT = 1;
    const TYPE_CASH = 2;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", nullable=false)
     */
    protected $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Bank")
     * @ORM\JoinColumn(name="bank_id", referencedColumnName="id", nullable=true)
     */
    protected $bank;

    /**
     * @var string
     * @ORM\Column(name="number", type="string", nullable=true)
     */
    private $number;

    /**
     * @var string
     * @ORM\Column(name="type", type="string", nullable=false)
     */
    private $type;

    public function __toString()
    {
        $string = '';

        if ($this->getBank()) {
            $string .= $this->getBank() . ' - ';
        }

        if ($this->getNumber()) {
            $string .= $this->getNumber() . ' - ';
        }

        $string .= $this->getTypeName() . ' ';

        return $string;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return PaymentMethod
     */
    public function setName(string $name): PaymentMethod
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * @param mixed $bank
     * @return PaymentMethod
     */
    public function setBank($bank)
    {
        $this->bank = $bank;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumber(): ?string
    {
        return $this->number;
    }

    /**
     * @param string $number
     * @return PaymentMethod
     */
    public function setNumber(string $number): PaymentMethod
    {
        $this->number = $number;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return PaymentMethod
     */
    public function setType(string $type): PaymentMethod
    {
        $this->type = $type;
        return $this;
    }

    public static function getTypeDictionary()
    {
        $type = [
            1 => 'Konto bankowe',
            2 => 'Gotówka',
        ];

        return $type;
    }

    public function getTypeName(): string
    {
        $type = self::getTypeDictionary();

        return isset($type[$this->getType()]) ? $type[$this->getType()] : 'unknown';
    }

}