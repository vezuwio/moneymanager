<?php

namespace App\Form;

use App\Entity\Account;
use App\Entity\Bank;
use App\Entity\Category;
use App\Entity\Expense;
use App\Entity\PaymentMethod;
use App\Repository\CategoryRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExpenseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('moneyAmount', NumberType::class, [
                'label' => 'Kwota',
            ])
            ->add('category', ChoiceType::class, [
                'label' => 'Kategoria',
            ])
            ->add('category', EntityType::class, [
                'label' => 'Kategoria',
                'class' => Category::class,
                'query_builder' => function (CategoryRepository $er) {
                    return $er->getByType(Category::TYPE_EXPENSE, Category::TYPE_COMMON);
                },
            ])
            ->add('paymentMethod', EntityType::class, [
                'label' => 'Forma płatności',
                'class' => PaymentMethod::class,
            ])
            ->add('transactionDate', DateTimeType::class, [
                'label' => 'Data transakcji',
                'data' => new \DateTime(),
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'datetimepicker'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Expense::class,
        ]);
    }
}
