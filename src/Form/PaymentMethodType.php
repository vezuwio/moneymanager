<?php

namespace App\Form;

use App\Entity\Account;
use App\Entity\Bank;
use App\Entity\PaymentMethod;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PaymentMethodType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder
            ->add('name', TextType::class, [
                'label' => 'Nazwa',
            ])
            ->add('number', TextType::class, [
                'label' => 'Numer',
            ])
            ->add('bank', EntityType::class, [
                'label' => 'Bank',
                'class' => Bank::class,
            ])
            ->add('type', ChoiceType::class, [
                'label' => 'Typ',
                'choices' => array_flip(PaymentMethod::getTypeDictionary()),
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PaymentMethod::class,
        ]);
    }
}
