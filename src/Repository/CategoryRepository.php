<?php

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Category::class);
    }

    public function getByType(string $type1, ?string $type2 = null)
    {
        $builder = $this->createQueryBuilder('c')
            ->where('c.type = :type1')
            ->setParameter('type1', $type1);
        if ($type2) {
            $builder->andWhere('c.type = :type2')
                ->setParameter('type2', $type2);
        }
            $builder->orderBy('c.name', 'ASC');

        $builder->getQuery()->getResult();
    }
}