<?php

namespace App\DTO\Assembler;

use App\DTO\DtoInterface;
use App\Entity\EntityInterface;

interface AssemblerInterface
{

    /**
     * @param DtoInterface $dto
     * @param EntityInterface|null $entity
     * @return EntityInterface
     */
    public function readDTO(DtoInterface $dto, ?EntityInterface $entity = null): EntityInterface;

    /**
     * @param DtoInterface $dto
     * @return EntityInterface
     */
    public function createEntity(DtoInterface $dto): EntityInterface;

    /**
     * @param EntityInterface $entity
     * @param DtoInterface $dto
     * @return EntityInterface
     */
    public function updateEntity(EntityInterface $entity, DtoInterface $dto): EntityInterface;

    /**
     * @param EntityInterface $entity
     * @return DtoInterface
     */
    public function writeDTO(EntityInterface $entity): DtoInterface;
}