<?php

namespace App\DTO\Assembler;

use App\DTO\DtoInterface;
use App\DTO\ExpenseDTO;
use App\Entity\EntityInterface;
use App\Entity\Expense;

class ExpenseAssembler implements AssemblerInterface
{

    /**
     * @param ExpenseDTO $dto
     * @param Expense|null $entity
     * @return Expense
     */
    public function readDTO(DtoInterface $dto, ?EntityInterface $entity = null): EntityInterface
    {
        if (!$entity) {
            $entity = new Expense(
                $dto->getUser(),
                $dto->getMoneyAmount()
            );
        } else {
            $entity->setUser($dto->getUser());
            $entity->setMonetAmount($dto->getMoneyAmount());
        }

        $entity ->setCategory($dto->getCategory());
        $entity ->setMoneyType($dto->getMoneyType());
        $entity ->setTransactionDate($dto->getTransactionDate());

        return $entity;

    }

    /**
     * @param ExpenseDTO $dto
     * @return Expense
     */
    public function createEntity(DtoInterface $dto): EntityInterface
    {
        $this->readDTO($dto);
    }

    /**
     * @param Expense $entity
     * @param ExpenseDTO $dto
     * @return Expense
     */
    public function updateEntity(EntityInterface $entity, DtoInterface $dto): EntityInterface
    {
        $this->readDTO($dto, $entity);
    }


    /**
     * @param Expense $entity
     * @return ExpenseDTO
     */
    public function writeDTO(EntityInterface $entity): DtoInterface
    {
        return new ExpenseDTO(
            $entity->getMoneyAmount(),
            $entity->getUser(),
            $entity->getCategory(),
            $entity->getMoneyType(),
            $entity->getTransactionDate()
        );
    }
}