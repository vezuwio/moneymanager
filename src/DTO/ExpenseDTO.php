<?php

namespace App\DTO;

use App\Entity\BudgetEntity;
use App\Entity\User;
use Entity\Category;
use Symfony\Component\Validator\Constraints as Assert;

class ExpenseDTO
{
    /**
     * @var float
     * @Assert\NotBlank()
     */
    private $moneyAmount;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Category
     */
    private $category;

    /**
     * @var string
     */
    private $moneyType;

    /**
     * @var \DateTime
     */
    private $transactionDate;

    public function __construct(
        float $moneyAmount,
        ?User $user = null,
        ?Category $category = null,
        ?string $moneyType = null,
        ?\DateTime $transactionDate = null
    )
    {
        $this->moneyAmount = $moneyAmount;
        $this->user = $user;
        $this->category = $category;
        $this->moneyType = $moneyType;
        $this->transactionDate = $transactionDate;
    }

    /**
     * @return User
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @return float
     */
    public function getMoneyAmount(): ?float
    {
        return $this->moneyAmount;
    }

    /**
     * @return Category
     */
    public function getCategory(): ?Category
    {
        return $this->category;
    }

    /**
     * @return string
     */
    public function getMoneyType(): ?string
    {
        return $this->moneyType;
    }

    /**
     * @return \DateTime
     */
    public function getTransactionDate(): ?\DateTime
    {
        return $this->transactionDate;
    }

    /**
     * @param Category $category
     * @return BudgetEntity
     */
    public function setCategory(Category $category): BudgetEntity
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @param string $moneyType
     * @return BudgetEntity
     */
    public function setMoneyType(string $moneyType): BudgetEntity
    {
        $this->moneyType = $moneyType;
        return $this;
    }

    /**
     * @param \DateTime $transactionDate
     * @return BudgetEntity
     */
    public function setTransactionDate(\DateTime $transactionDate): BudgetEntity
    {
        $this->transactionDate = $transactionDate;
        return $this;
    }
}